import requests 
from bs4 import BeautifulSoup
from csv import reader, writer
link = 'https://www.goodreads.com/quotes'    #input("enter link")
tag = input("enter tag")
#filename = open(tag+'_getquotes.txt','w') 
limit  = int(input('enter a limit of pages-> '))

def scrape_site(url, page):
    res = requests.get(url+page)
    soup = BeautifulSoup(res.text, "html.parser")
    quotes = soup.find_all(class_="quote")
#    is_next_page = soup.find("li", class_="next")
    with open(tag+'_getquotes.csw', "w") as file:
        csv_writer = writer(file)

        for quote in quotes:
            text = quote.find(class_="quoteText").text
            author = quote.find(class_="authorOrTitle").get_text()
#            author_url = quote.find("a")["href"]
#            book = quote.find(class_="book").get_text()
            csv_writer.writerow(text)


for pageno in range(0,limit+1):
   scrape_site(link, '?page='+str(pageno))



