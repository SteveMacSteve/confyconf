import requests as req
import pdb
from bs4 import BeautifulSoup
from csv import reader, writer
link = 'https://www.goodreads.com/quotes'    #input("enter link")
tag = 'all' #input("enter tag")
filename = open(tag+'_getquotes.txt','w') 
limit  = int(input('enter a limit of pages-> '))

def format_quote(quote):
    max_l_len = 55
    formatted_quote = str()
    j = 0
    s = 0 
    if len(quote)<=max_l_len:
        return quote
     
    for i in range(max_l_len, len(quote), max_l_len):
        while (' ' not in quote[i-j:i+j]) and (',' not in quote[i-j:i+j]) and ('.' not in quote[i-j:i+j]) and ('!' not in quote[i-j:i+j]) and ('?' not in quote[i-j:i+j]):
            j+=1
        if (',' in quote[i-j:i+j]) or ('.' in quote[i-j:i+j]) or ('!' in quote[i-j:i+j]) or ('?' in quote[i-j:i+j]):
            for symb in [',', '.', '!', '?']:
                try:
                    pos = quote[i-j:i+j].index(symb)+1
                    formatted_quote += (quote[s:i-j+pos] + '\n' + quote[i-j+pos:i+j])
                    s = i+j
                    j = 0
                    break                                                           
                except:
                    continue
        else:
            pos = quote[i-j:i+j].index(' ')+1
            formatted_quote += (quote[s:i-j+pos] + '\n' + quote[i-j+pos:i+j])
            s = i+j
            j = 0
    formatted_quote += quote[s:]
    return formatted_quote
             
with open(tag+'_getquotes.csw', "w") as file:
    csv_writer = writer(file, delimiter = '&')
    for pageno in range(0,limit+1):
        res =req.get(link+'?format=json&mobile_xhr=1&page=' + str(pageno))
        soup = BeautifulSoup(res.json()['content_html'], 'lxml')
        print(soup.prettify())

        for [quote,info] in zip(soup.find_all('blockquote','quoteBody'), soup.find_all('div', 'quoteDetails')):
            try:
                while True:
                    quote.br.replace_with('\n')
            except:#do nothing
                pass
            q = quote.text

            a = info.find(class_='quoteAuthor').get_text().strip('\n')
            try:
                b = info.find(class_='quoteBook').get_text().strip('\n')
            except:
                b = ''
            if '\n' not in q:
                q = format_quote(q)
            if not b:
                toprint = q  + '\n\n\t\t - ' + a + '\n\n\n\n'
            else:
                toprint = q + '\n\n\t\t - ' + a + b  + '\n\n\n\n'

            print(toprint)
            print(toprint, file=filename)
            csv_writer.writerow([a,b,q])
