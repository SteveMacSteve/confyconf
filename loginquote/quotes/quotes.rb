#!/usr/bin/env ruby
# quotes.rb
# Picks a random quote from quotes.txt and prints it to stdout.
# Quotes should be separated by double newlines (single empty lines) in the file.

# We need to define Array#sample if this version of Ruby is really old.
unless Array.respond_to? 'sample'
	class Array
		def sample
			self[rand(length)]
		end
	end
end

# Here we go!
puts "\n\n\n"
puts File.open("#{File.dirname __FILE__}/many_quotes.txt").read.split(/\n\n\n\n/).sample
puts "\n\n\n"
gets
