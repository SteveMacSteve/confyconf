#!/usr/bin/env bash
set -euo pipefail

printf "staff-net\nmartstef@staff-net.ethz.ch\nVSV8-TR99-TRI83" | sudo openconnect sslvpn.ethz.ch
