#!/usr/bin/env bash
set -euo pipefail

mount.cifs //ifu-srv-3.ethz.ch/HYDgroup ~/ifu/HYDgroup
mount.cifs //ifu-srv-3.ethz.ch/HYDstaff/Martinetti ~/ifu/Martinetti
