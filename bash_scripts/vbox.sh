#!/usr/bin/env bash
set -euo pipefail

sudo modprobe vboxdrv
sudo modprobe vboxnetadp
sudo modprobe vboxnetflt
VirtualBox
