#!/usr/bin/env bash
set -euo pipefail

echo -e 'power on\nconnect C0:28:8D:D6:E1:55' | bluetoothctl
sleep 2.5
echo -e 'exit' | bluetoothctl
