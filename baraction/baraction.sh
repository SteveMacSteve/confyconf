#!/bin/bash
# baraction.sh for spectrwm status bar

## DISK
hdd_root() {
  hdd="$(df -h | awk 'NR==4{print $3, $5}')"
  icon="+@fn=1;󰣇+@fn=0;"
  echo -e "$icon$hdd"
}

hdd_home() {
  hdd="$(df -h | awk 'NR==8{print $3, $5}')"
  icon="+@fn=1;󰻈+@fn=0;"
  echo -e "$icon$hdd"
}
## RAM
mem() {
  mem=`free --mebi | awk '/Mem/ {printf "%dMiB\n", $3}'`
#  echo -e "RAM: $mem"
#  echo -e "$mem"
  icon="+@fn=1;󰢷+@fn=0;"
  echo -e "$icon$mem"
}

 ## CPU
cpu() {
  read cpu a b c previdle rest < /proc/stat
  prevtotal=$((a+b+c+previdle))
  sleep 0.5
  read cpu a b c idle rest < /proc/stat
  total=$((a+b+c+idle))
  cpu=$((100*( (total-prevtotal) - (idle-previdle) ) / (total-prevtotal) ))
#  echo -e "CPU: $cpu%"
  icon="+@fn=1;󰌣+@fn=0;"
  echo -e "$icon$cpu%"
}
## Updates
upd() {
    t=`date +"%M"`
    if [[ "$t" == "00" ]] 
    then
        echo `yay -Qu | wc -l` > ~/baraction/nr_of_updates.txt
    fi
    upd=`head -n 1 ~/baraction/nr_of_updates.txt`
#    echo -e "UPD: $upd"
  icon="+@fn=1;󰀱+@fn=0;"
  echo -e "$icon$upd"
}
## VOLUME
vol() {
    vol=`amixer get Master | awk -F'[][]' 'END { print $6 }'`
    if [[ "$vol" == "off" ]]
    then
        : 
    else	
        vol=`amixer get Master | awk -F'[][]' 'END{ print $2 }' | sed 's/on://g'`
    fi 
#    echo -e "VOL: $vol"
#    echo -e "$vol"
    icon="+@fn=1;󰓃+@fn=0;"
    echo -e "$icon$vol"
}

## BATTERY
bat() {
    bat=`acpi | awk -F',' 'NR==1{print $2}'`
#    echo -e "$bat"
#    echo -e "BAT: $bat"
    icon="+@fn=1;󰂎+@fn=0;"
    echo -e "$icon$bat"
}

## CONNECTION
con() {
    con=`nmcli | awk 'NR==1{print $2}'`
#    echo -e "$con" 
    if [[ "$con" = "connected" ]]
    then	    
        icon="+@fn=1;󰠳++@fn=0;"
    else
        icon="+@fn=1;󰓣++@fn=0;"
    fi
    echo -e "$icon"
}

bat_nr=`acpi | awk -F',' 'NR==1{print $2}' | tr -dc '0-9'`

SLEEP_SEC=1
#loops forever outputting a line every SLEEP_SEC secs

# It seems that we are limited to how many characters can be displayed via
# the baraction script output. And the the markup tags count in that limit.
# So I would love to add more functions to this script but it makes the 
# echo output too long to display correctly.
while :; do
  if [ "$bat_nr" -lt 5 ]
  then
	    echo "+@fg=8; "$bat_nr"; Battery about to die... Plug in!"
  else
	    echo "+@fg=1;$(hdd_root)+@fg=0; | +@fg=2; $(hdd_home)+@fg=0; | +@fg=1;$(cpu)+@fg=0; | +@fg=2;$(mem)+@fg=0; | +@fg=1;$(vol)+@fg=0; | +@fg=2;$(bat)+@fg=0; | +@fg=1;$(upd)+@fg=0; | +@fg=2;$(con)+@fg=0; | "
  fi
  sleep $SLEEP_SEC
done

#echo "+@fg=1; +@fn=1;💻+@fn=0; 0xe003 $(cpu) +@fg=0; | +@fg=2; +@fn=1;💾+@fn=0; $(mem) +@fg=0; | +@fg=3; +@fn=1;💿+@fn=0; $(hdd_root) +@fg=0; | +fg=3; +@fn=1; +@fn=0; $(hdd_home) +@fg=0; | +@fg=4; +@fn=1;🔈+@fn=0; $(vol) +@fg=0; | +@fg=4; +@fn=1; +@fn=0; $(bat) +@fg=0; | $(con) | "
