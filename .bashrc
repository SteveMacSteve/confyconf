#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='\[\e[1;34m\]<<< \[\e[0;37m\]\u@\h \W \[\e[1;34m\]>>> \[\e[0;37m\]'

alias mv='mv -i' #!stefano added
alias rm='rm -i' #"
alias cp='cp -i' #"
alias rules='cat rules' #"
alias headphones='~/bash_scripts/BT_SONY.sh' #"
alias headphonesold='~/bash_scripts/BT_AKG.sh' #"
alias boom='~/bash_scripts/BT_BOOM.sh' #"
alias flip='~/bash_scripts/bt_flip.sh' #"
alias bose='~/bash_scripts/BT_BOSE.sh' #"
alias ifu='~/bash_scripts/mount_ifu.sh' #"
alias vpn='~/bash_scripts/vpn_eth.sh'
alias myconf-git='/usr/bin/git --git-dir=$HOME/.myconf/ --work-tree=$HOME' #"
alias shownow='~/bash_scripts/update_agenda.sh; echo 'Ciao...'; sleep 1; shutdown now'
alias vbox='~/bash_scripts/vbox.sh'
alias ua='~/bash_scripts/update_agenda.sh'
alias rn='python ~/Desktop/random_nr_genrator.py'
alias spectrwm='startx ~/.xinitrc spectrwm'
alias xfce='startx ~/.xinitrc xfce'
#set length of History entries
HISTSIZE=10000 #"

export EDITOR="emacsclient -t"                  # $EDITOR opens in terminal
export VISUAL="emacsclient -c -a emacs"         # $VISUAL opens in GUI mode

export TERM=xterm-256color 	#stefano added

#PATH adjustments
# doom emacs
export PATH=~/.emacs.d/bin:$PATH

# python (numpy and ipython installation gave warning=
export PATH=~/.local/bin:$PATH

# PFLOTRAN stuff
export PETSC_DIR=/home/stefano/builds/petsc
export PETSC_ARCH=arch-linux-c-debug
[ -f /opt/miniconda3/etc/profile.d/conda.sh ] && source /opt/miniconda3/etc/profile.d/conda.sh
