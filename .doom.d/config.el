;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Stefano Martinetti"
      user-mail-address "stefano.martinetti@sunrise.ch")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-vibrant)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.


;; inserted by SM
;; start server
;; do backups ~
;;; backup (bu) files should not be laced out through harddrive
(setq make-backup-files t)
(setq backup-directory-alist `(("." . "~/.emacs.bu")))
(setq backup-by-copying-when-linked t)
(setq delete-old-versions t
      kept-new-versions 4
      kept-old-versions 2
      version-control t)
;; "floating" ivy frame
(require 'ivy-posframe)
;; display at `ivy-posframe-style'
(setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display)))
;; (setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-frame-center)))
;; (setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-window-center)))
;; (setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-frame-bottom-left)))
;; (setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-window-bottom-left)))
;; (setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-frame-top-center)))
(ivy-posframe-mode 1)

;; email mu4e config
(add-load-path! "/usr/share/emacs/site-lisp/mu4e")
(require 'mu4e)
;; use mu4e for e-mail in emacs
(after! mu4e
  (setq mail-user-agent 'mu4e-user-agent)
  ;; get mail
  (setq
   mu4e-get-mail-command "offlineimap"   ;; or fetchmail, or ...
   mu4e-update-interval 300)             ;; update every 5 minutes
  ;; tell message-mode how to send mail
  (require 'smtpmail)
  (setq message-send-mail-function 'smtpmail-send-it)
  ;; if our mail server lives at smtp.example.org; if you have a local
  ;; mail-server, simply use 'localhost' here.
                                        ;(setq smtpmail-smtp-server "smtp2.sunrise.ch")
  ;; Send mail using SMTP on the mail submission port 465.
                                        ;(setq smtpmail-smtp-service 465)
  ;; " Main Account"
  (setq mu4e-sent-folder "/Personal/Sent"
        mu4e-drafts-folder "/Personal/Drafts"
        mu4e-trash-folder "/Personal/Trash"
        user-mail-address "stefano.martinetti@sunrise.ch"
        smtpmail-default-smtp-server "smtp2.sunrise.ch"
        smtpmail-local-domain "sunrise.ch"
        smtpmail-smtp-user "stefano.martinetti@sunrise.ch"
        smtpmail-smtp-server "smtp2.sunrise.ch"
        smtpmail-stream-type 'ssl
        smtpmail-smtp-service 465)
  ;; Account list
  (defvar my-mu4e-account-alist
    '(("Personal"
       (mu4e-sent-folder "/Personal/Sent")
       (mu4e-drafts-folder "/Personal/Drafts")
       (mu4e-trash-folder "/Personal/Trash")
       (user-mail-address "stefano.martinetti@sunrise.ch")
       (smtpmail-default-smtp-server "smtp2.sunrise.ch")
       (smtpmail-local-domain "sunrise.ch")
       (smtpmail-smtp-user "stefano.martinetti@sunrise.ch")
       (smtpmail-smtp-server "smtp2.sunrise.ch")
       (smtpmail-stream-type ssl)
       (smtpmail-smtp-service 465))
      ("Work"
       (mu4e-sent-folder "/Work/Sent Items")
       (mu4e-drafts-folder "/Work/Drafts")
       (mu4e-trash-folder "/Work/Deleted Items")
       (user-mail-address "martstef@ethz.ch")
       (smtpmail-default-smtp-server "mail.ethz.ch")
       (smtpmail-local-domain "ethz.ch")
       (smtpmail-smtp-user "martstef@ethz.ch")
       (smtpmail-smtp-server "mail.ethz.ch")
       (smtpmail-stream-type starttls)
       (smtpmail-smtp-service 587))))

  ;; function to set account with which email is composed
  (defun my-mu4e-set-account ()
    "Set the account for composing a message."
    (let* ((account
            (if mu4e-compose-parent-message
                (let ((maildir (mu4e-message-field mu4e-compose-parent-message :maildir)))
                  (string-match "/\\(.*?\\)/" maildir)
                  (match-string 1 maildir))
              (completing-read (format "Compose with account: (%s) "
                                       (mapconcat #'(lambda (var) (car var))
                                                  my-mu4e-account-alist "/"))
                               (mapcar #'(lambda (var) (car var)) my-mu4e-account-alist)
                               nil t nil nil (caar my-mu4e-account-alist))))
           (account-vars (cdr (assoc account my-mu4e-account-alist))))
      (if account-vars
          (mapc #'(lambda (var)
                    (set (car var) (cadr var)))
                account-vars)
        (error "No email account found"))))

  ;; add function to mu42-compose-pre-hook
  (add-hook 'mu4e-compose-pre-hook 'my-mu4e-set-account)

  ;; don't keep message buffers around
  (setq message-kill-buffer-on-exit t)
  ;; attachments go here
  (setq mu4e-attachment-dir  "~/Downloads/Mailatt")

  ;; don't save messages to Sent Messages, IMAP takes care of this
  (setq mu4e-sent-messages-behavior 'sent)

  ;; attempt to show images when viewing messages
  (setq mu4e-view-show-images t)
  ;; bookmark extension
  (add-to-list 'mu4e-bookmarks
  '( :name  "Yesterday's messages"
     :query "date:2d..1d"
     :key   ?y))

  (add-to-list 'mu4e-bookmarks
  '( :name "Personal Inbox"
     :query "maildir:/Personal/INBOX"
     :key ?P
     :hide t
     :hide-undread t))

  (add-to-list 'mu4e-bookmarks
  '( :name "Work Inbox"
     :query "maildir:/Work/INBOX"
     :key ?W
     :hide t
     :hide-unread t)))
;;; set specific browser to open links
(setq browse-url-browser-function 'browse-url-firefox)

;;; org-mode config
(after! org
  (setq org-agenda-files '("~/org/" "\.org$")))
;; pdftools orgmode integration
(use-package! org-pdftools
  :hook (org-mode . org-pdftools-setup-link))
(use-package! org-noter-pdftools
  :after org-noter
  :config
  (with-eval-after-load 'pdf-annot
    (add-hook 'pdf-annot-activate-handler-functions #'org-noter-pdftools-jump-to-note)))
;;; pdf-tools annotations keybindings
(map! :after pdf-tools
      :map (pdf-annot-minor-mode-map)
      (:desc "pdf-annotation-markup-all"
       :n "a n m" #'pdf-annot-add-markup-annotation)
      (:desc "pdf-annotation-highlight"
       :n "a n h" #'pdf-annot-add-highlight-markup-annotation)
      (:desc "pdf-annotation-strikeout"
       :n "a n x" #'pdf-annot-add-strikeout-markup-annotation)
      (:desc "pdf-annotation-squiggly"
       :n "a n s" #'pdf-annot-add-squiggly-markup-annotation)
      (:desc "pdf-annotation-underline"
       :n "a n u" #'pdf-annot-add-underline-markup-annotation)
      (:desc "pdf-annotation-text"
       :n "a n t" #'pdf-annot-add-text-annotation))

;;; additional packages
;; idle-highlight-mode
(add-hook! 'python-mode-hook (idle-highlight-mode t)) ; do this also for other languages
;;(setq-hook! 'python-mode-hook idle-highlight-mode t) ; probably setq-hook works only for packages already in doom...?
(setq-hook! python-mode python-shell-interpreter "~/.local/bin/ipython"
            python-shell-interpreter-args "--simple-prompt --pprint")

(defun run-pythons ()
  (interactive)
  (run-python nil 't 't))

;;; conda stuff
(use-package conda
  :ensure t
  :init
  (setq conda-anaconda-home (expand-file-name "~/miniconda3"))
  (setq conda-env-home-directory (expand-file-name "~/miniconda3"))
  )
